package com.kolyadruz.kingaqua_android.mvp.views;

import com.arellomobile.mvp.MvpView;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.OrderWithDicts;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.ProductDetail;

import java.util.List;

public interface ActiveOrdersView extends MvpView {

    void updateData(List<OrderWithDicts> orders);

}
