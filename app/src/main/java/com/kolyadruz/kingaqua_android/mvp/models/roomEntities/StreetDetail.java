package com.kolyadruz.kingaqua_android.mvp.models.roomEntities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.kolyadruz.kingaqua_android.db.converters.DateTimeConverter;

@Entity(tableName = "streets_dict")
public class StreetDetail {

    @PrimaryKey()
    public int id;
    public String name;
    public String parent;
    @ColumnInfo(name = "type_name")
    public String typeName;
    @ColumnInfo(name = "date_updated")
    @TypeConverters(DateTimeConverter.class)
    public String dateUpdated;

}
