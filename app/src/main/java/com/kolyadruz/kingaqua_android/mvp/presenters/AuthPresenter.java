package com.kolyadruz.kingaqua_android.mvp.presenters;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.google.gson.Gson;
import com.kolyadruz.kingaqua_android.Screens;
import com.kolyadruz.kingaqua_android.app.App;
import com.kolyadruz.kingaqua_android.db.DbDAO;
import com.kolyadruz.kingaqua_android.mvp.NetworkService;
import com.kolyadruz.kingaqua_android.mvp.models.CourierRequestBody;
import com.kolyadruz.kingaqua_android.mvp.models.DictionaryRequestBody;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.Courier;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.PaymentDetail;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.ProductDetail;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.StatusDetail;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.TimeDetail;
import com.kolyadruz.kingaqua_android.mvp.views.AuthView;
import com.kolyadruz.kingaqua_android.utils.MD5HashUtils;
import com.kolyadruz.kingaqua_android.utils.PrefUtils;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class AuthPresenter extends BasePresenter<AuthView>{

    public static final String TAG = "AuthPresenter";

    @Inject
    DbDAO dbDAO;

    @Inject
    NetworkService networkService;

    private String phoneWithMask;

    private DictionaryRequestBody dictionaryRequestBody = new DictionaryRequestBody();

    private String hash;

    private int isCourierEnabled;

    @Override
    protected void onFirstViewAttach() {
        App.getAppComponent().inject(this);
        super.onFirstViewAttach();

        hash = PrefUtils.getPrefs().getString("hash", "");
        dictionaryRequestBody.dateUpdated = "1990-01-01 00:00:00.000";

        Log.d(TAG, "onFirstViewAttach: ");
        checkHash();
    }

    private void checkHash() {

        String hash = PrefUtils.getPrefs().getString("hash", "");

        if (hash.isEmpty() || hash.equals("")) {
            getViewState().navigateToScreen(new Screens.PhoneScreen());
        } else {
            getByHash(hash);
        }

    }

    public void logIn(String phone, String pass) {

        getByHash(MD5HashUtils.md5(phone+pass));
        PrefUtils.getEditor().putString("phone", phone).commit();

    }

    private void getByHash(String hash) {

        Log.d(TAG, "getByHash: " + hash);

        CourierRequestBody requestBody = new CourierRequestBody();

        requestBody.setCmd("getByHash");
        requestBody.setHash(hash);

        getViewState().showLoadingProgress(true);

        networkService.courier(requestBody)
                .flatMap(new Function<Courier, ObservableSource<List<ProductDetail>>>() {
                    @Override
                    public ObservableSource<List<ProductDetail>> apply(Courier courier) {

                        isCourierEnabled = courier.isEnabled;

                        //courier.hash = hash;

                        Log.d(TAG, "COURIER: " + new Gson().toJson(courier) );;

                        courier.hash = hash;

                        dbDAO.insertCourierData(courier);

                        dictionaryRequestBody.name = "Products";

                        return networkService.productsDict(dictionaryRequestBody);
                    }
                })
                .flatMap(new Function<List<ProductDetail>, ObservableSource<List<PaymentDetail>>>() {
                    @Override
                    public ObservableSource<List<PaymentDetail>> apply(List<ProductDetail> products) {

                        dbDAO.insertProductDeatils(products);

                        dictionaryRequestBody.name = "Payments";

                        return networkService.paymentsDict(dictionaryRequestBody);
                    }
                })
                .flatMap(new Function<List<PaymentDetail>, ObservableSource<List<TimeDetail>>>() {
                    @Override
                    public ObservableSource<List<TimeDetail>> apply(List<PaymentDetail> paymentDetails) {

                        dbDAO.insertPaymentDeatils(paymentDetails);

                        dictionaryRequestBody.name = "Times";

                        return networkService.timesDict(dictionaryRequestBody);
                    }
                })
                .flatMap(new Function<List<TimeDetail>, ObservableSource<List<StatusDetail>>>() {
                    @Override
                    public ObservableSource<List<StatusDetail>> apply(List<TimeDetail> timeDetails) {

                        dbDAO.insertTimeDeatils(timeDetails);

                        dictionaryRequestBody.name = "Statuses";

                        return networkService.statusesDict(dictionaryRequestBody);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<StatusDetail>>() {
                    @Override
                    public void onComplete()
                    {
                        Log.d(TAG, "onCompleted: ");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: " + e.toString());
                        getViewState().showLoadingProgress(false);
                        getViewState().showConnectionErrorMessage();
                    }

                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<StatusDetail> statusDetails) {

                        Log.d(TAG, "onNext: " + new Gson().toJson(statusDetails));

                        if (isCourierEnabled == 1) {
                            PrefUtils.getEditor().putString("hash", hash).commit();
                            new Thread(() -> dbDAO.insertStatusDeatils(statusDetails)).start();

                            getViewState().showMain();
                        } else {
                            getViewState().showErrIDMessage();
                        }

                    }
                });
    }

    public void phoneNumberConfirmClicked(String phoneWithMask) {
        this.phoneWithMask = phoneWithMask;
        getViewState().navigateToScreen(new Screens.PassScreen(phoneWithMask));
    }

    public void onForgotClicked() {
        getViewState().navigateToScreen(new Screens.RestoreScreen(phoneWithMask));
    }
}
