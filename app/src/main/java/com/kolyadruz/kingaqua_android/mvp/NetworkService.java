package com.kolyadruz.kingaqua_android.mvp;

import com.kolyadruz.kingaqua_android.app.Api;
import com.kolyadruz.kingaqua_android.mvp.models.CourierRequestBody;
import com.kolyadruz.kingaqua_android.mvp.models.OrderRequestBody;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.Courier;
import com.kolyadruz.kingaqua_android.mvp.models.DictionaryRequestBody;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.Order;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.PaymentDetail;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.ProductDetail;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.StatusDetail;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.StreetDetail;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.TimeDetail;

import java.util.List;

import io.reactivex.Observable;


public class NetworkService {

    private Api api;

    public NetworkService(Api api) {
        this.api = api;
    }

    public Observable<Courier> courier(CourierRequestBody requestBody) {
        return api.courier(requestBody);
    }

    public Observable<List<Order>> orders(OrderRequestBody requestBody) {
        return api.orders(requestBody);
    }

    public Observable<List<ProductDetail>> productsDict(DictionaryRequestBody requestBody) {
        return api.products(requestBody);
    }

    public Observable<List<PaymentDetail>> paymentsDict(DictionaryRequestBody requestBody) {
        return api.payments(requestBody);
    }

    public Observable<List<StatusDetail>> statusesDict(DictionaryRequestBody requestBody) {
        return api.statuses(requestBody);
    }

    public Observable<List<StreetDetail>> streetsDict(DictionaryRequestBody requestBody) {
        return api.streets(requestBody);
    }

    public Observable<List<TimeDetail>> timesDict(DictionaryRequestBody requestBody) {
        return api.times(requestBody);
    }

}
