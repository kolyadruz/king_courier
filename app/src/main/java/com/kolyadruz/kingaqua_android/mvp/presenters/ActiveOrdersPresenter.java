package com.kolyadruz.kingaqua_android.mvp.presenters;

import com.arellomobile.mvp.InjectViewState;
import com.google.gson.Gson;
import com.kolyadruz.kingaqua_android.Screens;
import com.kolyadruz.kingaqua_android.app.App;
import com.kolyadruz.kingaqua_android.db.DbDAO;
import com.kolyadruz.kingaqua_android.mvp.models.Product;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.Courier;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.Order;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.OrderWithDicts;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.ProductDetail;
import com.kolyadruz.kingaqua_android.mvp.views.ActiveOrdersView;
import com.kolyadruz.kingaqua_android.ui.orders.fragments.TabContainer;
import com.kolyadruz.kingaqua_android.utils.PrefUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class ActiveOrdersPresenter extends BasePresenter<ActiveOrdersView> {

    @Inject
    DbDAO dbDAO;

    private Router router;

    public ActiveOrdersPresenter(Router router) {
        this.router = router;
    }

    private String hash;

    List<Integer> ordersIds = new ArrayList<>();

    @Override
    protected void onFirstViewAttach() {
        App.getAppComponent().inject(this);
        super.onFirstViewAttach();
        hash = PrefUtils.getPrefs().getString("hash", "");
        subscribeOnOrders();
    }

    private void subscribeOnOrders() {

        Disposable disposable = dbDAO.getCourierData(hash)
                .flatMap(new Function<Courier, Flowable<List<Order>>>() {
                    @Override
                    public Flowable<List<Order>> apply(Courier courier) {

                        return dbDAO.getActiveOrders(courier.id);
                    }
                })
                .flatMap(new Function<List<Order>, Flowable<List<OrderWithDicts>>>() {
                    @Override
                    public Flowable<List<OrderWithDicts>> apply(List<Order> orders) {

                        ordersIds.clear();

                        for(Order order: orders) {
                            ordersIds.add(order.id);
                        }

                        return dbDAO.getActiveOrdersWithDict(ordersIds);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<OrderWithDicts>>() {
                    @Override
                    public void accept(List<OrderWithDicts> orders) {
                       getViewState().updateData(orders);
                    }
                });

        unsubscribeOnDestroy(disposable);

    }

    public void showDetail(OrderWithDicts order) {

        List<ProductDetail> products = new ArrayList<>();

        List<Integer> ids = new ArrayList<>();

        for (Product product: order.data.products) {

           ids.add(product.id);
        }

        Disposable disposable = dbDAO.getOrderProducts(ids)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<ProductDetail>>() {
                    @Override
                    public void accept(List<ProductDetail> productDetails) {
                        //getViewState().showDetail(order, productDetails);

                        Gson gson = new Gson();
                        String orderStr = gson.toJson(order);
                        String productsStr = gson.toJson(productDetails);

                        router.navigateTo(new Screens.OrderDetailScreen(TabContainer.ACTIVE_ORDERS, orderStr, productsStr));
                    }
                });

        unsubscribeOnDestroy(disposable);
    }
}
