package com.kolyadruz.kingaqua_android.mvp.models.roomEntities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.kolyadruz.kingaqua_android.db.converters.DateTimeConverter;

@Entity(tableName = "times_dict")
public class TimeDetail {

    @PrimaryKey()
    public int id;
    public String name;
    @ColumnInfo(name = "time_start")
    public String timeStart;
    @ColumnInfo(name = "time_end")
    public String timeEnd;
    @ColumnInfo(name = "date_created")
    @TypeConverters(DateTimeConverter.class)
    public String dateCreated;
    @ColumnInfo(name = "date_updated")
    @TypeConverters(DateTimeConverter.class)
    public String dateUpdated;

}
