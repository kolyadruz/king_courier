package com.kolyadruz.kingaqua_android.mvp.views;

import com.arellomobile.mvp.MvpView;

import ru.terrakok.cicerone.Screen;

public interface AuthView extends BaseMvpView {

    void showMain();

    void navigateToScreen(Screen screen);

    void showLoadingProgress(boolean show);

}
