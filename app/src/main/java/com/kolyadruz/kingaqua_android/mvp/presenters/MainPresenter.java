package com.kolyadruz.kingaqua_android.mvp.presenters;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.google.gson.Gson;
import com.kolyadruz.kingaqua_android.app.App;
import com.kolyadruz.kingaqua_android.db.DbDAO;
import com.kolyadruz.kingaqua_android.mvp.NetworkService;
import com.kolyadruz.kingaqua_android.mvp.models.CourierRequestBody;
import com.kolyadruz.kingaqua_android.mvp.models.OrderRequestBody;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.Courier;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.Order;
import com.kolyadruz.kingaqua_android.mvp.views.MainView;
import com.kolyadruz.kingaqua_android.utils.PrefUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;


@InjectViewState
public class MainPresenter extends BasePresenter<MainView> {

    private static final String TAG = "MainPresenter";

    @Inject
    DbDAO dbDAO;

    @Inject
    NetworkService networkService;

    private int currentCourierID;

    private String hash;
    private CourierRequestBody requestBody = new CourierRequestBody();
    private OrderRequestBody orderRequestBody = new OrderRequestBody();

    private boolean onSwitchResponse = false;

    @Override
    protected void onFirstViewAttach() {
        App.getAppComponent().inject(this);
        super.onFirstViewAttach();

        hash = PrefUtils.getPrefs().getString("hash", "");

        subscribeOnCourierData();
        getOrdersWithInterval();
    }

    private void getOrdersWithInterval() {
        Disposable disposable = Observable.interval(0, 5000, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::getOrders);
        unsubscribeOnDestroy(disposable);
    }

    private void getOrders(Long lng) {

        requestBody.setHash(hash);
        requestBody.setCmd("getByHash");

        //Log.d(TAG, "getOrders: hash=" + hash);

        networkService.courier(requestBody)
                .flatMap(new Function<Courier, ObservableSource<List<Order>>>() {
                    @Override
                    public ObservableSource<List<Order>> apply(Courier courier) {

                        courier.hash = hash;
                        dbDAO.insertCourierData(courier);

                        orderRequestBody.setCmd("getCourier");
                        orderRequestBody.setCourierId(courier.id);
                        orderRequestBody.setHash(hash);
                        orderRequestBody.setHashType("courier");

                        return networkService.orders(orderRequestBody);
                    }
                })
                //.interval(0, 5, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Order>>() {
                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onCompleted: getOrders()");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: getOrders() " + e.toString());
                    }

                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<Order> orders) {
                        //Log.d(TAG, "onNext: ");

                        Log.d(TAG, "onNext: " + new Gson().toJson(orders));

                        new Thread(() -> dbDAO.insertOrders(orders)).start();

                    }

                });

    }

    public void onlineSwitched(boolean isOnline) {

        onSwitchResponse = true;

        unsubscribeNow();

        requestBody.setCmd("updOnline");
        requestBody.setId(currentCourierID);
        requestBody.setHash(hash);
        requestBody.setIsOnline((short)(isOnline? 1 : 0));

        networkService.courier(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Courier>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d(TAG, "onSubscribe: hash = " + hash);
                    }

                    @Override
                    public void onNext(Courier courier) {
                        Log.d(TAG, "onNext: " + (courier.isOnline == 1? "online" : "offline"));
                        //getViewState().toggleSwitch();

                        courier.hash = hash;
                        new Thread(() -> dbDAO.insertCourierData(courier)).start();

                        if (courier.isOnline == 1) {
                            getViewState().startTracking();
                        } else {
                            getViewState().stopTracking();
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        onSwitchResponse = false;
                        Log.d(TAG, "onError: updOnline FAULT: " + e.toString());

                        getOrdersWithInterval();
                        subscribeOnCourierData();

                        getViewState().enableSwitch();
                    }

                    @Override
                    public void onComplete() {
                        onSwitchResponse = false;
                        Log.d(TAG, "onComplete: ");

                        getOrdersWithInterval();
                        subscribeOnCourierData();

                        getViewState().enableSwitch();
                    }
                });

    }

    private void subscribeOnCourierData() {

        Disposable disposable = dbDAO.getCourierData(hash)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Courier>() {
                    @Override
                    public void accept(Courier courier) {
                        //for (Courier courier : courier) {
                            currentCourierID = courier.id;
                            if (!onSwitchResponse) {
                                //getViewState().toggleSwitch(courier.isOnline == 1);

                                if (courier.isOnline == 1) {
                                    getViewState().toggleSwitch(true);
                                    //getViewState().startTracking();
                                } else {
                                    //getViewState().stopTracking();
                                }

                            }

                        //}
                    }
                });

        unsubscribeOnDestroy(disposable);

    }
}
