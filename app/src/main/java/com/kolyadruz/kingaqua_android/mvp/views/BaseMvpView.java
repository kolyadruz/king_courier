package com.kolyadruz.kingaqua_android.mvp.views;

import com.arellomobile.mvp.MvpView;

public interface BaseMvpView extends MvpView {

    void showErrIDMessage();
    void showConnectionErrorMessage();

}
