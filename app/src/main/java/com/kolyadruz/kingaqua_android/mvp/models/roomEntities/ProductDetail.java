package com.kolyadruz.kingaqua_android.mvp.models.roomEntities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.kolyadruz.kingaqua_android.db.converters.DateTimeConverter;

import java.util.Date;

@Entity(tableName = "products_dict")
public class ProductDetail {

    @PrimaryKey()
    public int id;
    public String name;
    public Float price;
    @ColumnInfo(name = "price_express")
    public Float priceExpress;
    public int saleCount;
    @ColumnInfo(name = "sale_count_price")
    public Float saleCountPrice;
    @ColumnInfo(name = "bomb_price")
    public Float bombPrice;
    public int ord;
    public String description;
    @ColumnInfo(name = "date_created")
    @TypeConverters(DateTimeConverter.class)
    public String dateCreated;
    @ColumnInfo(name = "date_updated")
    @TypeConverters(DateTimeConverter.class)
    public String dateUpdated;

}
