package com.kolyadruz.kingaqua_android.mvp.models.roomEntities;

import androidx.room.Embedded;
import androidx.room.Relation;

public class OrderWithDicts {

    @Embedded
    public Order data;

    @Relation(parentColumn =  "time_id", entityColumn = "id")
    public TimeDetail timeDetail;

    @Relation(parentColumn = "payment_id", entityColumn = "id")
    public PaymentDetail paymentDetail;

}
