package com.kolyadruz.kingaqua_android.mvp.presenters;

import android.content.Intent;
import android.net.Uri;

import com.arellomobile.mvp.InjectViewState;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kolyadruz.kingaqua_android.app.App;
import com.kolyadruz.kingaqua_android.db.DbDAO;
import com.kolyadruz.kingaqua_android.mvp.NetworkService;
import com.kolyadruz.kingaqua_android.mvp.models.OrderRequestBody;
import com.kolyadruz.kingaqua_android.mvp.models.Product;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.Order;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.OrderWithDicts;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.ProductDetail;
import com.kolyadruz.kingaqua_android.mvp.views.OrderDetailView;
import com.kolyadruz.kingaqua_android.ui.orders.fragments.TabContainer;
import com.kolyadruz.kingaqua_android.utils.PrefUtils;

import org.reactivestreams.Publisher;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class OrderDetailPresenter extends BasePresenter<OrderDetailView> {

    @Inject
    DbDAO dbDAO;

    @Inject
    NetworkService networkService;

    private Router router;

    private String TYPE;

    private String hash;

    private OrderWithDicts currentOrder;
    private List<ProductDetail> products;

    public OrderDetailPresenter(Router router, String TabContainerTAG, String orderStr, String productsStr) {

        this.router = router;

        this.TYPE = TabContainerTAG;

        Gson gson = new Gson();

        currentOrder = gson.fromJson(orderStr, OrderWithDicts.class);
        products = gson.fromJson(productsStr, new TypeToken<ArrayList<ProductDetail>>(){}.getType());
    }

    @Override
    protected void onFirstViewAttach() {
        App.getAppComponent().inject(this);
        super.onFirstViewAttach();
        hash = PrefUtils.getPrefs().getString("hash", "");
        showData();

        if (TYPE == TabContainer.ACTIVE_ORDERS) {
            subscribeOnActiveOrderChange();
        }
    }

    private void subscribeOnActiveOrderChange() {

        Disposable disposable = dbDAO.getOrderByID(currentOrder.data.id)
                .flatMap(new Function<Order, Publisher<OrderWithDicts>>() {
                    @Override
                    public Publisher<OrderWithDicts> apply(Order order) {
                        return dbDAO.getOrderWithDictByID(currentOrder.data.id);
                    }
                }).flatMap(new Function<OrderWithDicts, Publisher<List<ProductDetail>>>() {
                    @Override
                    public Publisher<List<ProductDetail>> apply(OrderWithDicts order) {

                        if (order.data.statusId == 2) {
                            currentOrder = order;
                        } else {
                            router.exit();
                        }

                        List<Integer> ids = new ArrayList<>();

                        for (Product product: order.data.products) {
                            ids.add(product.id);
                        }

                        return dbDAO.getOrderProducts(ids);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<ProductDetail>>() {
                    @Override
                    public void accept(List<ProductDetail> productDetails) {

                        products = productDetails;
                        showData();

                    }
                });

                unsubscribeOnDestroy(disposable);

    }

    private void showData() {
        getViewState().showData(TYPE.equals(TabContainer.ACTIVE_ORDERS), currentOrder, products);
    }

    public void onCompleteClicked() {
        setOrderStatus(3);
    }

    public void onCancelClicked() {
        setOrderStatus(5);
    }

    private void setOrderStatus(int statusId) {

        OrderRequestBody requestBody = new OrderRequestBody();
        requestBody.setCourierId(currentOrder.data.courierId);
        requestBody.setHash(hash);
        requestBody.setHashType("courier");
        requestBody.setCmd("complete");
        //requestBody.setStatusId(statusId);

        networkService.orders(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Order>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<Order> orders) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    public void onBackPressed() {
        router.exit();
    }

    public void onCallClicked() {

        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel: " + currentOrder.data.customerId));

        getViewState().startIntent(intent);

    }
}
