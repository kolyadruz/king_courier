package com.kolyadruz.kingaqua_android.mvp.models.roomEntities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.kolyadruz.kingaqua_android.db.converters.DateTimeConverter;

import java.util.Date;

import retrofit2.http.Streaming;

@Entity(tableName = "payments_dict")
public class PaymentDetail {

    @PrimaryKey()
    public int id;
    public String name;
    @ColumnInfo(name = "date_created")
    @TypeConverters(DateTimeConverter.class)
    public String dateCreated;
    @ColumnInfo(name = "date_updated")
    @TypeConverters(DateTimeConverter.class)
    public String dateUpdated;

}
