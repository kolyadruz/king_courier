package com.kolyadruz.kingaqua_android.mvp.presenters;

import androidx.annotation.NonNull;

import com.arellomobile.mvp.MvpPresenter;
import com.arellomobile.mvp.MvpView;
import com.kolyadruz.kingaqua_android.utils.ErrorMessageUtils;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class BasePresenter<View extends MvpView> extends MvpPresenter<View> {
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    protected void unsubscribeOnDestroy(@NonNull Disposable disposable) {
        compositeDisposable.add(disposable);
    }

    @Override public void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
    }

    protected String getErrorMessage(int err_id) {
        return ErrorMessageUtils.getErrorMessage(err_id);
    }

    protected void unsubscribeNow() {
        compositeDisposable.clear();
    }

}
