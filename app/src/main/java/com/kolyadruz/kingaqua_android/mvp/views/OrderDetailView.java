package com.kolyadruz.kingaqua_android.mvp.views;

import android.content.Intent;

import com.arellomobile.mvp.MvpView;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.OrderWithDicts;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.ProductDetail;

import java.util.List;

public interface OrderDetailView extends MvpView {

    void showData(boolean show, OrderWithDicts order, List<ProductDetail> products);

    void startIntent(Intent intent);

}
