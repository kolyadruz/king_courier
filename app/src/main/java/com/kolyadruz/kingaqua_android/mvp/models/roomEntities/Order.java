package com.kolyadruz.kingaqua_android.mvp.models.roomEntities;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.kolyadruz.kingaqua_android.db.converters.DateTimeConverter;
import com.kolyadruz.kingaqua_android.db.converters.ProductsConverter;
import com.kolyadruz.kingaqua_android.mvp.models.Product;

import java.util.List;

@Entity(tableName = "orders")
public class Order {

    @Ignore
    public String cmd;
    @PrimaryKey()
    public int id;
    @ColumnInfo(name = "street_name")
    public String streetName;
    public String house;
    public String entrance;
    @Nullable
    public short lvl;
    @Nullable
    public short apartment;
    @ColumnInfo(name = "is_express")
    public short isExpress;
    @Nullable
    @ColumnInfo(name = "address_id")
    public int addressId;
    @ColumnInfo(name = "customer_id")
    public long customerId;
    @ColumnInfo(name = "date_created")
    @TypeConverters(DateTimeConverter.class)
    public String dateCreated;
    @ColumnInfo(name = "date_distributed")
    @TypeConverters(DateTimeConverter.class)
    public String dateDistributed;
    @ColumnInfo(name = "date_completed")
    @TypeConverters(DateTimeConverter.class)
    public String dateCompleted;
    @ColumnInfo(name = "min_passed")
    public int minPassed;
    @ColumnInfo(name = "time_id")
    public int timeId;
    @ColumnInfo(name = "payment_id")
    public int paymentId;
    @ColumnInfo(name = "status_id")
    public int statusId;
    @ColumnInfo(name = "employee_id")
    public int employeeId;
    @ColumnInfo(name = "courier_id")
    public int courierId;
    public double lat;
    public double lon;
    public String dsc;
    @ColumnInfo(name = "date_start")
    @TypeConverters(DateTimeConverter.class)
    public String dateStart;
    @ColumnInfo(name = "date_end")
    @TypeConverters(DateTimeConverter.class)
    public String dateEnd;
    @Ignore
    public String hashType;
    @Ignore
    public String hash;
    @ColumnInfo(name = "street_id")
    public int streetId;
    public String source;
    public String amount;
    @Ignore
    public String lotId;
    @TypeConverters(ProductsConverter.class)
    public List<Product> products;

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getEntrance() {
        return entrance;
    }

    public void setEntrance(String entrance) {
        this.entrance = entrance;
    }

    public short getLvl() {
        return lvl;
    }

    public void setLvl(short lvl) {
        this.lvl = lvl;
    }

    public short getApartment() {
        return apartment;
    }

    public void setApartment(short apartment) {
        this.apartment = apartment;
    }

    public short getIsExpress() {
        return isExpress;
    }

    public void setIsExpress(short isExpress) {
        this.isExpress = isExpress;
    }

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getDateDistributed() {
        return dateDistributed;
    }

    public void setDateDistributed(String dateDistributed) {
        this.dateDistributed = dateDistributed;
    }

    public String getDateCompleted() {
        return dateCompleted;
    }

    public void setDateCompleted(String dateCompleted) {
        this.dateCompleted = dateCompleted;
    }

    public int getMinPassed() {
        return minPassed;
    }

    public void setMinPassed(int minPassed) {
        this.minPassed = minPassed;
    }

    public int getTimeId() {
        return timeId;
    }

    public void setTimeId(int timeId) {
        this.timeId = timeId;
    }

    public int getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public int getCourierId() {
        return courierId;
    }

    public void setCourierId(int courierId) {
        this.courierId = courierId;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(Float lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(Float lon) {
        this.lon = lon;
    }

    public String getDsc() {
        return dsc;
    }

    public void setDsc(String dsc) {
        this.dsc = dsc;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getHashType() {
        return hashType;
    }

    public void setHashType(String hashType) {
        this.hashType = hashType;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public int getStreetId() {
        return streetId;
    }

    public void setStreetId(int streetId) {
        this.streetId = streetId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getLotId() {
        return lotId;
    }

    public void setLotId(String lotId) {
        this.lotId = lotId;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
