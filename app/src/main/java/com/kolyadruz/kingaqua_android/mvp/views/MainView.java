package com.kolyadruz.kingaqua_android.mvp.views;

import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.Order;

import java.util.List;

public interface MainView extends BaseMvpView {

    void updateData(List<Order> orders);

    void toggleSwitch(boolean isChecked);

    void enableSwitch();

    void requestGpsPermission();

    void startTracking();
    void stopTracking();

}
