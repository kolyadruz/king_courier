package com.kolyadruz.kingaqua_android.mvp.models.roomEntities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.kolyadruz.kingaqua_android.db.converters.DateTimeConverter;

@Entity(tableName = "statuses_dict")
public class StatusDetail {

    @PrimaryKey()
    public int id;
    public String name;
    @ColumnInfo(name = "date_created")
    @TypeConverters(DateTimeConverter.class)
    public String dateCreated;
    @ColumnInfo(name = "date_updated")
    @TypeConverters(DateTimeConverter.class)
    public String dateUpdated;

}

