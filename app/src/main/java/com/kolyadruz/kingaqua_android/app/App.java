package com.kolyadruz.kingaqua_android.app;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import androidx.room.Room;

import com.kolyadruz.kingaqua_android.di.AppComponent;
import com.kolyadruz.kingaqua_android.db.AppDatabase;
import com.kolyadruz.kingaqua_android.di.DaggerAppComponent;
import com.kolyadruz.kingaqua_android.di.DaggerNavigationComponent;
import com.kolyadruz.kingaqua_android.di.NavigationComponent;
import com.kolyadruz.kingaqua_android.di.modules.ContextModule;
import com.kolyadruz.kingaqua_android.di.modules.RoomModule;

public class App  extends Application {

    private static AppComponent sAppComponent;
    private static NavigationComponent sNavigationComponent;

    //private AppDatabase database;

    @Override
    public void onCreate() {
        super.onCreate();

        sAppComponent = DaggerAppComponent.builder().contextModule(new ContextModule(this)).roomModule(new RoomModule(this)).build();
        sNavigationComponent = DaggerNavigationComponent.create();



        //database = Room.databaseBuilder(this, AppDatabase.class, "database").build();
    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    @VisibleForTesting
    public static void setAppComponent(@NonNull AppComponent appComponent) {
        sAppComponent = appComponent;
    }

    public static NavigationComponent getNavigationComponent() {
        return sNavigationComponent;
    }

}
