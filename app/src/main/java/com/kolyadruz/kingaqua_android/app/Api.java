package com.kolyadruz.kingaqua_android.app;

import com.kolyadruz.kingaqua_android.mvp.models.CourierRequestBody;
import com.kolyadruz.kingaqua_android.mvp.models.OrderRequestBody;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.Courier;
import com.kolyadruz.kingaqua_android.mvp.models.DictionaryRequestBody;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.Order;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.PaymentDetail;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.ProductDetail;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.StatusDetail;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.StreetDetail;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.TimeDetail;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface Api {

    @POST("courier")
    Observable<Courier> courier(@Body CourierRequestBody requestBody);

    @POST("order")
    Observable<List<Order>> orders(@Body OrderRequestBody requestBody);

    @POST("dictionary")
    Observable<List<ProductDetail>> products(@Body DictionaryRequestBody requestBody);

    @POST("dictionary")
    Observable<List<PaymentDetail>> payments(@Body DictionaryRequestBody requestBody);

    @POST("dictionary")
    Observable<List<TimeDetail>> times(@Body DictionaryRequestBody requestBody);

    @POST("dictionary")
    Observable<List<StatusDetail>> statuses(@Body DictionaryRequestBody requestBody);

    @POST("dictionary")
    Observable<List<StreetDetail>> streets(@Body DictionaryRequestBody requestBody);

}
