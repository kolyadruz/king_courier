package com.kolyadruz.kingaqua_android.ui.auth.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kolyadruz.kingaqua_android.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class Restore extends Fragment {

    private static final String PHONE_NUMBER_PARAM1 = "phoneNumber";

    @BindView(R.id.phoneTV)
    TextView phone;
    @BindView(R.id.emailTV)
    TextView email;

    private String maskedPhoneNumber;

    Unbinder unbinder;

    public static Restore getNewInstance(String phoneNumber) {
        Restore fragment = new Restore();
        Bundle args = new Bundle();
        args.putString(PHONE_NUMBER_PARAM1, phoneNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            maskedPhoneNumber = getArguments().getString(PHONE_NUMBER_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_restore, container, false);

        unbinder = ButterKnife.bind(this, rootView);

        phone.setText(maskedPhoneNumber);

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) unbinder.unbind();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


}
