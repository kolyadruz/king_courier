package com.kolyadruz.kingaqua_android.ui.orders.fragments;

public interface BackButtonListener {
    boolean onBackPressed();
}
