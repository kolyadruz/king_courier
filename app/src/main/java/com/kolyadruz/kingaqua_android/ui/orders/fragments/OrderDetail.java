package com.kolyadruz.kingaqua_android.ui.orders.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.kolyadruz.kingaqua_android.R;
import com.kolyadruz.kingaqua_android.adapters.ActiveOrderProductsAdapter;
import com.kolyadruz.kingaqua_android.app.App;
import com.kolyadruz.kingaqua_android.mvp.models.Product;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.OrderWithDicts;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.ProductDetail;
import com.kolyadruz.kingaqua_android.mvp.presenters.OrderDetailPresenter;
import com.kolyadruz.kingaqua_android.mvp.views.OrderDetailView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class OrderDetail extends MvpAppCompatFragment implements OrderDetailView, BackButtonListener {

    private static final String TAG = "OrderDetail";

    private static final String TAB_CONTAINER_TAG = "type";
    private static final String ORDER_PARAM = "order";
    private static final String PRODUCTS_PARAM = "products";

    @InjectPresenter
    OrderDetailPresenter presenter;

    @ProvidePresenter
    OrderDetailPresenter orderDetailPresenter() {
        return new OrderDetailPresenter(
                ((RouterProvider)getParentFragment()).getRouter(),
                getArguments().getString(TAB_CONTAINER_TAG),
                getArguments().getString(ORDER_PARAM),
                getArguments().getString(PRODUCTS_PARAM)
        );
    }

    @BindView(R.id.controlsLayout)
    LinearLayout controlsLayout;
    @BindView(R.id.orderNumber)
    TextView orderNumber;
    @BindView(R.id.orderTime)
    TextView orderTime;
    @BindView(R.id.orderType)
    TextView orderType;
    @BindView(R.id.orderAddress)
    TextView orderAddress;
    @BindView(R.id.orderEntrance)
    TextView orderEntrance;
    @BindView(R.id.orderFloor)
    TextView orderFloor;
    @BindView(R.id.orderApart)
    TextView orderApart;
    @BindView(R.id.orderExtraInfo)
    TextView orderExtraInfo;
    @BindView(R.id.orderWaterType)
    TextView orderWaterType;
    @BindView(R.id.orderBottlesCount)
    TextView orderBottlesCount;
    @BindView(R.id.orderPaymentType)
    TextView orderPaymentType;
    @BindView(R.id.orderAmount)
    TextView orderAmount;

    ActiveOrderProductsAdapter adapter = new ActiveOrderProductsAdapter();

    Unbinder unbinder;

    public static OrderDetail getNewInstance(String TabContainerTAG, String orderStr, String productsStr) {
        OrderDetail fragment = new OrderDetail();
        Bundle args = new Bundle();
        args.putString(TAB_CONTAINER_TAG, TabContainerTAG);
        args.putString(ORDER_PARAM, orderStr);
        args.putString(PRODUCTS_PARAM, productsStr);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_order_detail, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        return rootView;
    }

    @OnClick(R.id.callBtn)
    void onCallClicked() {
        presenter.onCallClicked();
    }

    @OnClick(R.id.completeBtn)
    void onCompleteClicked() {
        presenter.onCompleteClicked();
    }

    @OnClick(R.id.cancelBtn)
    void onCancelClicked() {
        presenter.onCancelClicked();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) unbinder.unbind();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public boolean onBackPressed() {

        Log.d(TAG, "onBackPressed: ");
        
        presenter.onBackPressed();
        return true;
    }

    @Override
    public void showData(boolean show, OrderWithDicts order, List<ProductDetail> products) {
        controlsLayout.setVisibility(show? View.VISIBLE : View.GONE);

        orderNumber.setText("Заказ " + order.data.id);
        orderTime.setText(order.data.dateCreated.substring(order.data.dateCreated.indexOf('T') + 1, order.data.dateCreated.indexOf('T') + 6) );
        orderType.setText(order.data.isExpress == 1? "Экспресс" : "Стандарт");
        orderAddress.setText(order.data.streetName + ", " + order.data.house);
        orderEntrance.setText(""+order.data.entrance);
        orderFloor.setText(""+order.data.lvl);
        orderApart.setText(""+order.data.apartment);
        orderExtraInfo.setText(order.data.dsc);
        //orderWaterType;

        for (Product product: order.data.products) {
            if (product.id == 1) {
                orderBottlesCount.setText("" + product.quantity);
            }
        }

        orderPaymentType.setText(order.paymentDetail.name);
        orderAmount.setText(order.data.amount + " руб.");

    }

    @Override
    public void startIntent(Intent intent) {
        if (ContextCompat.checkSelfPermission(App.getAppComponent().getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 123);
        } else {
            startActivity(intent);
        }
    }
}
