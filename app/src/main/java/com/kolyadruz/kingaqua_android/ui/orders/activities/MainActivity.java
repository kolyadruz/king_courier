package com.kolyadruz.kingaqua_android.ui.orders.activities;

import android.Manifest;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.kolyadruz.kingaqua_android.BuildConfig;
import com.kolyadruz.kingaqua_android.R;
import com.kolyadruz.kingaqua_android.Screens;
import com.kolyadruz.kingaqua_android.app.App;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.Order;
import com.kolyadruz.kingaqua_android.mvp.presenters.MainPresenter;
import com.kolyadruz.kingaqua_android.mvp.views.MainView;
import com.kolyadruz.kingaqua_android.services.BackgroundLocationService;
import com.kolyadruz.kingaqua_android.ui.orders.fragments.BackButtonListener;
import com.kolyadruz.kingaqua_android.utils.GpsUtils;
import com.kolyadruz.kingaqua_android.utils.PrefUtils;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;
import ru.terrakok.cicerone.Screen;
import ru.terrakok.cicerone.android.support.SupportAppNavigator;
import ru.terrakok.cicerone.commands.Command;
import ru.terrakok.cicerone.commands.Replace;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class MainActivity extends MvpAppCompatActivity implements MainView,  NavigationView.OnNavigationItemSelectedListener, SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String TAG = "MainActivity";

    final int TASK1_CODE = 1;
    final int TASK2_CODE = 2;
    final int TASK3_CODE = 3;

    public final static int STATUS_START = 100;
    public final static int STATUS_FINISH = 200;

    public final static String PARAM_TIME = "time";
    public final static String PARAM_TASK = "task";
    public final static String PARAM_RESULT = "result";
    public final static String PARAM_STATUS = "status";

    public final static String BROADCAST_ACTION = "ru.startandroid.develop.p0961servicebackbroadcast";

    private final int REQUEST_PERMISSIONS_REQUEST_CODE = 200;

    @Inject
    Router router;

    @Inject
    NavigatorHolder navigatorHolder;

    @InjectPresenter()
    MainPresenter presenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.nav_view)
    NavigationView navView;

    ActionBarDrawerToggle toggle;

    private RadioGroup onlineSwitch;

    private MyReceiver myReceiver;
    private boolean mBound = false;
    private BackgroundLocationService mService = null;

    public Navigator navigator = new SupportAppNavigator(this, R.id.nav_host_fragment) {
        @Override
        protected void applyCommand(Command command) {
            super.applyCommand(command);
            getSupportFragmentManager().executePendingTransactions();
        }
    };

    // Monitors the state of the connection to the service.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            BackgroundLocationService.LocalBinder binder = (BackgroundLocationService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        App.getNavigationComponent().inject(this);

        super.onCreate(savedInstanceState);
        myReceiver = new MyReceiver();
        setContentView(R.layout.activity_main);

        if (GpsUtils.requestingLocationUpdates(this)) {
            if (!checkPermissions()) {
                requestPermissions();
            }
        }

        ButterKnife.bind(this);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setSupportActionBar(toolbar);

        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.syncState();
        navView.setNavigationItemSelectedListener(this);
        drawer.addDrawerListener(toggle);

        if (savedInstanceState == null) {
            navigator.applyCommands(new Command[]{new Replace(
                    new Screens.BottomNavScreen()
                )
            });

            navView.setCheckedItem(R.id.nav_home);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        PrefUtils.getPrefs()
                .registerOnSharedPreferenceChangeListener(this);

        /*mRequestLocationUpdatesButton = (Button) findViewById(R.id.request_location_updates_button);
        mRemoveLocationUpdatesButton = (Button) findViewById(R.id.remove_location_updates_button);

        mRequestLocationUpdatesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!checkPermissions()) {
                    requestPermissions();
                } else {
                    mService.requestLocationUpdates();
                }
            }
        });

        mRemoveLocationUpdatesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mService.removeLocationUpdates();
            }
        });

        // Restore the state of the buttons when the activity (re)launches.
        setButtonsState(GpsUtils.requestingLocationUpdates(this));*/

        // Bind to the service. If the service is in foreground mode, this signals to the service
        // that since this activity is in the foreground, the service can exit foreground mode.
        bindService(new Intent(this, BackgroundLocationService.class), mServiceConnection,
                Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver,
                new IntentFilter(BackgroundLocationService.ACTION_BROADCAST));
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver);
        navigatorHolder.removeNavigator();
        super.onPause();
    }

    @Override
    protected void onStop() {
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            unbindService(mServiceConnection);
            mBound = false;
        }
        PrefUtils.getPrefs()
                .unregisterOnSharedPreferenceChangeListener(this);
        super.onStop();
    }

    private boolean checkPermissions() {
        return  PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            Snackbar.make(
                    findViewById(R.id.activity_main),
                    R.string.permission_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    })
                    .show();
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission was granted.
                mService.requestLocationUpdates();
            } else {
                // Permission denied.

                toggleSwitch(false);
                Snackbar.make(
                        findViewById(R.id.activity_main),
                        R.string.permission_denied_explanation,
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.settings, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        })
                        .show();
            }
        }
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra(BackgroundLocationService.EXTRA_LOCATION);
            if (location != null) {
                //Toast.makeText(MainActivity.this, GpsUtils.getLocationText(location),Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        // Update the buttons state depending on whether location updates are being requested.
        if (s.equals(GpsUtils.KEY_REQUESTING_LOCATION_UPDATES)) {
            toggleSwitch(sharedPreferences.getBoolean(GpsUtils.KEY_REQUESTING_LOCATION_UPDATES,
                    false));
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            return;
        }

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);

        if (fragment != null && fragment instanceof BackButtonListener && ((BackButtonListener) fragment).onBackPressed()) {

            Log.d(TAG, "onBackPressed: RETURNED");

            return;
        }


        Log.d(TAG, "onBackPressed: SUPER");

        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        onlineSwitch = menu.findItem(R.id.myswitch).getActionView().findViewById(R.id.onlineToggle);
        onlineSwitch.findViewById(R.id.online).setOnClickListener(v -> {
            Log.d(TAG, "onClickedSwitch: online");

            disableSwitch();
            presenter.onlineSwitched(true);
        });

        onlineSwitch.findViewById(R.id.offline).setOnClickListener(v -> {
            Log.d(TAG, "onClickedSwitch: offline");

            disableSwitch();
            presenter.onlineSwitched(false);
        });

        return true;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nav_home:
                switchPage(new Screens.BottomNavScreen());
                break;
            case R.id.nav_gallery:
                switchPage(new Screens.GalleryScreen());
                break;
            case R.id.nav_slideshow:
                switchPage(new Screens.SlideShowScreen());
                break;
            case R.id.nav_tools:
                //switchPage(new Screens.ToolsScreen());
                PrefUtils.getEditor().putString("hash", "").commit();
                router.newRootScreen(new Screens.AuthScreen());
                break;
            case R.id.nav_share:
                switchPage(new Screens.ShareScreen());
                break;
            case R.id.nav_send:
                switchPage(new Screens.SendScreen());
                break;
        }

        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    public void switchPage(Screen screen) {
        navigator.applyCommands(new Command[]{new Replace(screen)});
    }

    @Override
    public void updateData(List<Order> orders) {
    }

    @Override
    public void toggleSwitch(boolean isOnline) {
        if (onlineSwitch == null) {
            return;
        }

        if (isOnline) {
            if (onlineSwitch.getCheckedRadioButtonId() == R.id.offline) {
                onlineSwitch.check(R.id.online);
            }
        } else {
            if (onlineSwitch.getCheckedRadioButtonId() == R.id.online) {
                onlineSwitch.check(R.id.offline);
            }
        }

    }

    @Override
    public void enableSwitch() {
        onlineSwitch.findViewById(R.id.online).setEnabled(true);
        onlineSwitch.findViewById(R.id.offline).setEnabled(true);
    }

    @Override
    public void requestGpsPermission() {
        //ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
    }

    private void disableSwitch() {
        onlineSwitch.findViewById(R.id.online).setEnabled(false);
        onlineSwitch.findViewById(R.id.offline).setEnabled(false);
    }

    @Override
    public void showErrIDMessage() {

    }

    @Override
    public void showConnectionErrorMessage() {

    }

    @Override
    public void startTracking() {
        if (!checkPermissions()) {
            requestPermissions();
        } else {
            mService.requestLocationUpdates();
        }
    }

    @Override
    public void stopTracking() {
        Log.d(TAG, "stopTracking: ");
        mService.removeLocationUpdates();
    }

}
