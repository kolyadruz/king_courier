package com.kolyadruz.kingaqua_android.ui.auth.fragments;

import android.os.Bundle;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.PresenterType;
import com.kolyadruz.kingaqua_android.R;
import com.kolyadruz.kingaqua_android.mvp.presenters.AuthPresenter;
import com.kolyadruz.kingaqua_android.mvp.views.AuthView;

import br.com.sapereaude.maskedEditText.MaskedEditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ru.terrakok.cicerone.Screen;

public class PhoneNumber extends MvpAppCompatFragment implements AuthView {

    private static final String TAG = "PhoneNumber";

    @InjectPresenter(type = PresenterType.WEAK, tag = AuthPresenter.TAG)
    AuthPresenter presenter;

    @BindView(R.id.phoneET)
    MaskedEditText phone;

    @BindView(R.id.confirmBtn)
    Button confirmBtn;

    Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_phone_number, container, false);

        unbinder = ButterKnife.bind(this, rootView);

        phone.addTextChangedListener(phoneWathcer);

        return rootView;
    }

    @OnClick(R.id.confirmBtn)
    void onConfirmClicked() {
        presenter.phoneNumberConfirmClicked(phone.getText().toString());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) unbinder.unbind();
    }

    private final TextWatcher phoneWathcer = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            Log.d(TAG, "afterTextChanged: " + s.length());
            if (s.length() == 17) {
                confirmBtn.setEnabled(true);
            } else {
                confirmBtn.setEnabled(false);
            }
        }
    };

    @Override
    public void showMain() {

    }

    @Override
    public void navigateToScreen(Screen screen) {

    }

    @Override
    public void showLoadingProgress(boolean show) {

    }

    @Override
    public void showErrIDMessage() {

    }

    @Override
    public void showConnectionErrorMessage() {

    }
}
