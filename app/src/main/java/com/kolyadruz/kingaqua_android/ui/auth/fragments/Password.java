package com.kolyadruz.kingaqua_android.ui.auth.fragments;

import android.os.Bundle;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.PresenterType;
import com.kolyadruz.kingaqua_android.R;
import com.kolyadruz.kingaqua_android.mvp.presenters.AuthPresenter;
import com.kolyadruz.kingaqua_android.mvp.views.AuthView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ru.terrakok.cicerone.Screen;

public class Password extends MvpAppCompatFragment implements AuthView {

    private static final String TAG = "Password";

    private static final String PHONE_NUMBER_PARAM = "phoneNumber";

    @BindView(R.id.loadingView)
    RelativeLayout loadingView;
    @BindView(R.id.phoneTV)
    TextView phone;
    @BindView(R.id.passET)
    EditText pass;
    @BindView(R.id.confirmBtn)
    Button confirmBtn;
    @BindView(R.id.forgotBtn)
    TextView forgotBtn;

    @InjectPresenter(type = PresenterType.WEAK, tag = AuthPresenter.TAG)
    AuthPresenter presenter;

    private String maskedPhoneNumber;

    Unbinder unbinder;

    public static Password getNewInstance(String phoneNumber) {
        Password fragment = new Password();
        Bundle args = new Bundle();
        args.putString(PHONE_NUMBER_PARAM, phoneNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            maskedPhoneNumber = getArguments().getString(PHONE_NUMBER_PARAM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_password, container, false);

        unbinder = ButterKnife.bind(this, rootView);

        phone.setText(maskedPhoneNumber);
        pass.addTextChangedListener(passWathcer);

        return rootView;
    }

    @OnClick(R.id.confirmBtn)
    void onConfirmClicked() {
        String clearPhone = phone.getText().toString().replaceAll("[\\(\\)\\-\\+]","");
        clearPhone = "8"+clearPhone.substring(1);
        presenter.logIn(clearPhone, pass.getText().toString());
    }

    @OnClick(R.id.forgotBtn)
    void onForgotClicked() {
        presenter.onForgotClicked();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) unbinder.unbind();
    }

    private final TextWatcher passWathcer = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            Log.d(TAG, "afterTextChanged: " + s.length());
            if (s.length() > 2) {
                confirmBtn.setEnabled(true);
            } else {
                confirmBtn.setEnabled(false);
            }
        }
    };

    @Override
    public void showErrIDMessage() {

    }

    @Override
    public void showConnectionErrorMessage() {

    }

    @Override
    public void showMain() {

    }

    @Override
    public void navigateToScreen(Screen screen) {

    }

    @Override
    public void showLoadingProgress(boolean show) {

        //loadingView.setVisibility(show? View.VISIBLE : View.GONE);

        forgotBtn.setVisibility(show? View.INVISIBLE : View.VISIBLE);

        pass.setEnabled(!show);
        confirmBtn.setEnabled(!show);
    }

}
