package com.kolyadruz.kingaqua_android.ui.auth;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.PresenterType;
import com.kolyadruz.kingaqua_android.R;
import com.kolyadruz.kingaqua_android.Screens;
import com.kolyadruz.kingaqua_android.app.App;
import com.kolyadruz.kingaqua_android.mvp.presenters.AuthPresenter;
import com.kolyadruz.kingaqua_android.mvp.views.AuthView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;
import ru.terrakok.cicerone.Screen;
import ru.terrakok.cicerone.android.support.SupportAppNavigator;
import ru.terrakok.cicerone.commands.Command;
import ru.terrakok.cicerone.commands.Forward;

public class Auth extends MvpAppCompatActivity implements AuthView {

    @Inject
    NavigatorHolder navigatorHolder;

    @Inject
    Router router;

    @InjectPresenter(type = PresenterType.WEAK, tag = AuthPresenter.TAG)
    AuthPresenter presenter;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private Navigator navigator = new SupportAppNavigator(this, R.id.container);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        App.getNavigationComponent().inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        ButterKnife.bind(this);

    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        navigatorHolder.removeNavigator();
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void showErrIDMessage() {

    }

    @Override
    public void showConnectionErrorMessage() {

    }

    @Override
    public void showMain() {
        router.replaceScreen(new Screens.MainScreen());
    }

    @Override
    public void navigateToScreen(Screen screen) {
        navigator.applyCommands(new Command[] {
                new Forward(screen)
        });
    }

    @Override
    public void showLoadingProgress(boolean show) {
        progressBar.setVisibility(show? View.VISIBLE : View.GONE);
    }

}
