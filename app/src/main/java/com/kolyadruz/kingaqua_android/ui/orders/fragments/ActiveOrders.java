package com.kolyadruz.kingaqua_android.ui.orders.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.kolyadruz.kingaqua_android.R;
import com.kolyadruz.kingaqua_android.adapters.OnOrderClickedListener;
import com.kolyadruz.kingaqua_android.adapters.ActiveOrdersAdapter;
import com.kolyadruz.kingaqua_android.app.App;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.OrderWithDicts;
import com.kolyadruz.kingaqua_android.mvp.presenters.ActiveOrdersPresenter;
import com.kolyadruz.kingaqua_android.mvp.views.ActiveOrdersView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ActiveOrders extends MvpAppCompatFragment implements ActiveOrdersView, OnOrderClickedListener {

    @InjectPresenter
    ActiveOrdersPresenter presenter;

    @ProvidePresenter
    ActiveOrdersPresenter activeOrdersPresenter() {
        return new ActiveOrdersPresenter(((RouterProvider)getParentFragment()).getRouter());
    }

    private static final String TAG = "NewOrders";

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.ordersCountTV)
    TextView ordersCount;
    @BindView(R.id.ordersListView)
    RecyclerView newOrdersList;

    private Unbinder unbinder;

    private ActiveOrdersAdapter adapter = new ActiveOrdersAdapter(this);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_active_orders, container, false);

        unbinder = ButterKnife.bind(this, rootView);

        LinearLayoutManager llm = new LinearLayoutManager(App.getAppComponent().getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        newOrdersList.setLayoutManager(llm);
        newOrdersList.setAdapter(adapter);

        title.setText("Активные заказы:");

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) unbinder.unbind();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onOrderClicked(OrderWithDicts order) {
        presenter.showDetail(order);
    }

    @Override
    public void updateData(List<OrderWithDicts> orders) {
        ordersCount.setText(""+orders.size());
        adapter.updateData(orders);
    }
}
