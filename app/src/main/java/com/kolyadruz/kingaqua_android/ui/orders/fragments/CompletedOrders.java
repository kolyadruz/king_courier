package com.kolyadruz.kingaqua_android.ui.orders.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.kolyadruz.kingaqua_android.R;
import com.kolyadruz.kingaqua_android.adapters.CompletedOrdersAdapter;
import com.kolyadruz.kingaqua_android.adapters.OnOrderClickedListener;
import com.kolyadruz.kingaqua_android.app.App;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.OrderWithDicts;
import com.kolyadruz.kingaqua_android.mvp.presenters.CompletedOrdersPresenter;
import com.kolyadruz.kingaqua_android.mvp.views.CompletedOrdersView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class CompletedOrders extends MvpAppCompatFragment implements CompletedOrdersView, OnOrderClickedListener {

    @InjectPresenter
    CompletedOrdersPresenter presenter;

    @ProvidePresenter
    CompletedOrdersPresenter completedOrdersPresenter() {
        return new CompletedOrdersPresenter(((RouterProvider)getParentFragment()).getRouter());
    }

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.ordersCountTV)
    TextView completedOrdersCount;
    @BindView(R.id.ordersListView)
    RecyclerView completedOrdersList;

    private Unbinder unbinder;

    private CompletedOrdersAdapter adapter = new CompletedOrdersAdapter(this);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_completed_orders, container, false);

        unbinder = ButterKnife.bind(this, rootView);

        LinearLayoutManager llm = new LinearLayoutManager(App.getAppComponent().getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        completedOrdersList.setLayoutManager(llm);
        completedOrdersList.setAdapter(adapter);

        title.setText("Завершенные заказы:");

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) unbinder.unbind();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void updateData(List<OrderWithDicts> orders) {
        completedOrdersCount.setText("" + orders.size());
        adapter.updateData(orders);
    }

    @Override
    public void onOrderClicked(OrderWithDicts order) {
        presenter.showDetail(order);
    }
}
