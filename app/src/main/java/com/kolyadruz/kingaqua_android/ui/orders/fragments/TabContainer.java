package com.kolyadruz.kingaqua_android.ui.orders.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.kolyadruz.kingaqua_android.R;
import com.kolyadruz.kingaqua_android.Screens;
import com.kolyadruz.kingaqua_android.app.App;
import com.kolyadruz.kingaqua_android.di.modules.subnavigation.LocalCiceroneHolder;
import com.kolyadruz.kingaqua_android.mvp.views.OrderDetailView;

import javax.inject.Inject;

import ru.terrakok.cicerone.Cicerone;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.Router;
import ru.terrakok.cicerone.android.support.SupportAppNavigator;

public class TabContainer extends MvpAppCompatFragment implements RouterProvider, BackButtonListener {

    private static final String TAG = "TabContainer";

    public static final String ACTIVE_ORDERS = "active";
    public static final String BOMB_ORDERS = "bomb";
    public static final String COMPLETED_ORDERS = "completed";


    private static final String EXTRA_NAME = "tcf_extra_name";

    private Navigator navigator;

    @Inject
    LocalCiceroneHolder ciceroneHolder;

    public static TabContainer getNewInstance(String name) {
        TabContainer fragment = new TabContainer();

        Bundle arguments = new Bundle();
        arguments.putString(EXTRA_NAME, name);
        fragment.setArguments(arguments);

        return fragment;
    }

    private String getContainerName() {
        return getArguments().getString(EXTRA_NAME);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        App.getNavigationComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

    private Cicerone<Router> getCicerone() {
        return ciceroneHolder.getCicerone(getContainerName());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tab_container, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getChildFragmentManager().findFragmentById(R.id.ftc_container) == null) {

            switch (getContainerName()) {
                case ACTIVE_ORDERS:
                    getCicerone().getRouter().replaceScreen(new Screens.ActiveOrdersScreen());
                    break;
                case BOMB_ORDERS:
                    getCicerone().getRouter().replaceScreen(new Screens.BombOrdersScreen());
                    break;
                case COMPLETED_ORDERS:
                    getCicerone().getRouter().replaceScreen(new Screens.CompletedOrdersScreen());
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getCicerone().getNavigatorHolder().setNavigator(getNavigator());
    }

    @Override
    public void onPause() {
        getCicerone().getNavigatorHolder().removeNavigator();
        super.onPause();
    }

    private Navigator getNavigator() {
        if (navigator == null) {
            navigator = new SupportAppNavigator(getActivity(), getChildFragmentManager(), R.id.ftc_container);
        }
        return navigator;
    }

    @Override
    public Router getRouter() {
        return getCicerone().getRouter();
    }

    @Override
    public boolean onBackPressed() {

        Log.d(TAG, "onBackPressed: " + getContainerName());

        Fragment fragment = getChildFragmentManager().findFragmentById(R.id.ftc_container);
        if (fragment != null
                && fragment instanceof OrderDetailView
                && ((BackButtonListener) fragment).onBackPressed()) {
            return true;
        } else {
            //((RouterProvider) getParentFragment()).getRouter().exit();
            return false;
        }
    }

}
