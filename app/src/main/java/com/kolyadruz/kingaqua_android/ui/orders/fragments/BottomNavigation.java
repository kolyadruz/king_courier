package com.kolyadruz.kingaqua_android.ui.orders.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.ashokvarma.bottomnavigation.BottomNavigationBar;
import com.ashokvarma.bottomnavigation.BottomNavigationItem;
import com.kolyadruz.kingaqua_android.R;
import com.kolyadruz.kingaqua_android.Screens;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ru.terrakok.cicerone.Router;

public class BottomNavigation extends MvpAppCompatFragment implements RouterProvider, BackButtonListener {

    private static final String TAG = "BottomNavigation";

    @Inject
    Router router;

    @BindView(R.id.ab_bottom_navigation_bar)
    BottomNavigationBar bottomNavigationBar;

    Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.activity_bottom_navigation, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        initViews();

        if (savedInstanceState == null) {
            bottomNavigationBar.selectTab(0, true);
        }

        return rootView;
    }

    private void initViews() {

        bottomNavigationBar.setBarBackgroundColor(R.color.darkBlue);

        bottomNavigationBar
                .addItem(new BottomNavigationItem(R.drawable.ic_cli, "Заказы")
                        .setInActiveColorResource(R.color.transperent_white)
                        .setActiveColorResource(R.color.white))
                .addItem(new BottomNavigationItem(R.drawable.ic_b, "Бомбы")
                        .setInActiveColorResource(R.color.transperent_white)
                        .setActiveColorResource(R.color.white))
                .addItem(new BottomNavigationItem(R.drawable.ic_cli, "Выполненные")
                        .setInActiveColorResource(R.color.transperent_white)
                        .setActiveColorResource(R.color.white))
                .initialise();

        bottomNavigationBar.setTabSelectedListener(new BottomNavigationBar.OnTabSelectedListener() {
            @Override
            public void onTabSelected(int position) {
                switch (position) {
                    case 0:
                        selectTab("ANDROID", position);
                        break;
                    case 1:
                        selectTab("BUG", position);
                        break;
                    case 2:
                        selectTab("DOG", position);
                        break;
                }
                bottomNavigationBar.selectTab(position, false);
            }

            @Override
            public void onTabUnselected(int position) {

            }

            @Override
            public void onTabReselected(int position) {
                onTabSelected(position);
            }
        });

    }

    private void selectTab(String tabTAG, int tabID) {
        FragmentManager fm = getChildFragmentManager();
        Fragment currentFragment = null;
        List<Fragment> fragments = fm.getFragments();
        if (fragments != null) {
            for (Fragment f : fragments) {
                if (f.isVisible()) {
                    currentFragment = f;
                    break;
                }
            }
        }

        Fragment newFragment = fm.findFragmentByTag(tabTAG);

        if (currentFragment != null && newFragment != null && currentFragment == newFragment) {
            Log.d(TAG, "selectTab: fragment FOUND");
            return;
        }

        Log.d(TAG, "selectTab: fragment NOT FOUND");

        FragmentTransaction transaction = fm.beginTransaction();
        if (newFragment == null) {
            switch (tabID) {
                case 0:
                    transaction.add(R.id.bottom_nav_host_fragment, new Screens.TabContainerScreen(TabContainer.ACTIVE_ORDERS).getFragment() ,tabTAG);//new Screens.ActiveOrdersScreen().getFragment(), tabTAG);
                    break;
                case 1:
                    transaction.add(R.id.bottom_nav_host_fragment, new Screens.TabContainerScreen(TabContainer.BOMB_ORDERS).getFragment() ,tabTAG);//BombOrdersScreen().getFragment(), tabTAG);
                    break;
                case 2:
                    transaction.add(R.id.bottom_nav_host_fragment, new Screens.TabContainerScreen(TabContainer.COMPLETED_ORDERS).getFragment() ,tabTAG);//CompletedOrdersScreen().getFragment(), tabTAG);
                    break;
            }

        }

        if (currentFragment != null) {
            transaction.hide(currentFragment);
        }

        if (newFragment != null) {
            transaction.show(newFragment);
        }
        transaction.commitNow();
    }

    @Override
    public Router getRouter() {
        return router;
    }

    @Override
    public boolean onBackPressed() {

        Log.d(TAG, "onBackPressed: ");
        
        /*FragmentManager fm = getChildFragmentManager();
        Fragment fragment = null;
        List<Fragment> fragments = fm.getFragments();
        if (fragments != null) {
            for (Fragment f : fragments) {
                if (f.isVisible()) {
                    fragment = f;
                    break;
                }
            }
        }
        if (fragment != null
                && fragment instanceof BackButtonListener
                && ((BackButtonListener) fragment).onBackPressed()) {
            return true;
        } else {
            return true;
        }*/

        FragmentManager fm = getChildFragmentManager();
        Fragment currentFragment = null;
        List<Fragment> fragments = fm.getFragments();
        if (fragments != null) {
            for (Fragment f : fragments) {
                if (f.isVisible()) {
                    currentFragment = f;
                    break;
                }
            }
        }

        if (currentFragment != null
                && currentFragment instanceof BackButtonListener
                && ((BackButtonListener) currentFragment).onBackPressed()) {
            return true;
        } else {
            return false;
        }
    }
}
