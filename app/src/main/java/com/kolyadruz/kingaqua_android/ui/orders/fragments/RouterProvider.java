package com.kolyadruz.kingaqua_android.ui.orders.fragments;

import ru.terrakok.cicerone.Router;

interface RouterProvider {
    Router getRouter();
}
