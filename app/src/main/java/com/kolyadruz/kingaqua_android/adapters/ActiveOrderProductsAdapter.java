package com.kolyadruz.kingaqua_android.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.kolyadruz.kingaqua_android.R;
import com.kolyadruz.kingaqua_android.db.DbDAO;
import com.kolyadruz.kingaqua_android.mvp.NetworkService;
import com.kolyadruz.kingaqua_android.mvp.models.Product;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.Order;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.OrderWithDicts;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.ProductDetail;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActiveOrderProductsAdapter extends RecyclerView.Adapter<ActiveOrderProductsAdapter.MyViewHolder> {

    @Inject
    NetworkService networkService;

    @Inject
    DbDAO dbDAO;

    private final static int HEADER_ITEM = 1;
    private final static int PRODUCT_ITEM = 2;

    public static OrderWithDicts mOrder = new OrderWithDicts();
    public static List<Product> products = new ArrayList<>();
    public static List<ProductDetail> productDetails = new ArrayList<>();

    static boolean isSelfDataChanging = false;

    public ActiveOrderProductsAdapter() {
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.nameTV)
        public TextView nameTV;
        @BindView(R.id.countTV)
        public TextView countTV;
        @BindView(R.id.amountTV)
        public TextView amountTV;

        @BindView(R.id.plusBtn)
        public Button plsuBtn;

        @BindView(R.id.minusBtn)
        public Button minusBtn;

        private int quantity = products.get(getAdapterPosition()).quantity;

        public MyViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        @OnClick(R.id.plusBtn)
        void onPlusClicked() {
            isSelfDataChanging = true;
            disableButtons();
            quantity += 1;
            mOrder.data.products.get(getAdapterPosition()).quantity = quantity;
            onDataChange(mOrder.data);
        }

        @OnClick(R.id.minusBtn)
        void onMinusClicked() {
            isSelfDataChanging = true;
            disableButtons();
            if (quantity > 0) {
                quantity += 1;
                mOrder.data.products.get(getAdapterPosition()).quantity = quantity;
            } else {
                List<Product> newProducts = mOrder.data.products;
                newProducts.remove(getAdapterPosition());

                mOrder.data.products = newProducts;
            }
            onDataChange(mOrder.data);
        }

        private void disableButtons() {
            plsuBtn.setEnabled(false);
            minusBtn.setEnabled(false);
        }

    }

    public void onDataChange(Order order) {

        order.setCmd("complete");

        /*networkService.orders(order)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Order>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<Order> orders) {
                        dbDAO.insertOrders(orders);
                        isSelfDataChanging = false;
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });*/

    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public ActiveOrderProductsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_active_order_product, parent, false);

        if (viewType == HEADER_ITEM) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_products_header, parent, false);
        }

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        if (holder.getItemViewType() == HEADER_ITEM) return;

        ProductDetail productDetail = productDetails.get(position - 1);
        Product product = products.get(position - 1);

        holder.nameTV.setText(productDetail.name);
        holder.countTV.setText(product.quantity);

        holder.amountTV.setText(productDetail.price + " руб.");

    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HEADER_ITEM;
        } else {
            return PRODUCT_ITEM;
        }
    }

    @Override
    public int getItemCount() {
        if (productDetails != null) return productDetails.size() + 1;
        else return 0;
    }

    public void updateData(OrderWithDicts order, List<ProductDetail> products) {
        if (!isSelfDataChanging) {
            this.mOrder = order;
            this.products = order.data.products;
            this.productDetails = products;

            notifyDataSetChanged();
        }
    }
}


