package com.kolyadruz.kingaqua_android.adapters;

import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.OrderWithDicts;

public interface OnOrderClickedListener {
    void onOrderClicked(OrderWithDicts order);
}