package com.kolyadruz.kingaqua_android.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.kolyadruz.kingaqua_android.R;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.OrderWithDicts;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CompletedOrdersAdapter extends RecyclerView.Adapter<CompletedOrdersAdapter.MyViewHolder> {

    public static List<OrderWithDicts> mOrders = new ArrayList<>();

    private OnOrderClickedListener onOrderClickedListener;

    public CompletedOrdersAdapter(OnOrderClickedListener onOrderClickedListener) {
        this.onOrderClickedListener = onOrderClickedListener;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.addressTV)
        public TextView addressTV;
        @BindView(R.id.entranceTV)
        public TextView entranceTV;
        @BindView(R.id.floorTV)
        public TextView floorTv;
        @BindView(R.id.apartTV)
        public TextView apartTV;

        @BindView(R.id.waterTypeTV)
        public TextView waterTypeTV;
        @BindView(R.id.bottlesCountTV)
        public TextView bottlesCountTV;
        @BindView(R.id.paymentTypeTV)
        public TextView paymentTypeTV;

        @BindView(R.id.productsLayout)
        public LinearLayout productsLayout;

        @BindView(R.id.amountTV)
        public TextView amountTV;

        @BindView(R.id.orderTypeLayout)
        public RelativeLayout orderTypeLayout;
        @BindView(R.id.orderExpressTimeTV)
        public TextView orderExpressTimeTV;
        @BindView(R.id.orderTypeTV)
        public TextView orderTypeTV;
        @BindView(R.id.orderTimeTV)
        public TextView orderTimeTV;

        OnOrderClickedListener onPostClickedListener;

        public MyViewHolder(View v, OnOrderClickedListener onPostClickedListener) {
            super(v);

            ButterKnife.bind(this, v);

            this.onPostClickedListener = onPostClickedListener;
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onPostClickedListener.onOrderClicked(mOrders.get(getAdapterPosition()));
        }
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public CompletedOrdersAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order, parent, false);
        return new MyViewHolder(itemView, onOrderClickedListener);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        OrderWithDicts order = mOrders.get(position);

        holder.addressTV.setText(""+order.data.streetName + ", " + order.data.house);
        holder.entranceTV.setText(order.data.entrance);
        holder.floorTv.setText(""+order.data.lvl);
        holder.apartTV.setText(""+order.data.apartment);

        //holder.waterTypeTV.setText(order.);
        //holder.bottlesCountTV.setText(order.);

        //holder.bottlesCountTV.setText("" + order.data.products.get();

        holder.productsLayout.setVisibility(order.data.products.size() > 0? View.VISIBLE : View.GONE);

        holder.paymentTypeTV.setText(""+order.paymentDetail.name);

        holder.orderExpressTimeTV.setText(""+order.data.minPassed);

        //holder.orderTimeTV.setVisibility(order.data.timeId == 0? View.GONE : View.VISIBLE);
        //if (order.data.timeId != 0) {
            holder.orderTimeTV.setText(order.data.dateCreated.substring(order.data.dateCreated.indexOf('T') + 1, order.data.dateCreated.indexOf('T') + 6));
        //}

        //holder.productsLayout.setVisibility(order.products.size() > 0? View.VISIBLE : View.GONE);
        holder.amountTV.setText(order.data.amount);

        switch (order.data.statusId) {
            case 2:
                holder.orderExpressTimeTV.setVisibility(order.data.isExpress == 1? View.VISIBLE : View.GONE);

                if (order.data.isExpress == 1) {
                    holder.orderTypeTV.setText("Экспресс");
                } else {
                    holder.orderTypeTV.setText("Cтандарт");
                    if (order.timeDetail != null) {
                        holder.orderTypeTV.setText(order.timeDetail.name);
                    }
                }

                holder.orderTypeLayout.setBackgroundResource(order.data.isExpress == 1? R.color.darkYellow : R.color.orderItemGray);
                break;
            case 3:
                holder.orderExpressTimeTV.setVisibility(View.GONE);
                holder.orderTypeTV.setText("Выполнен");
                holder.orderTypeLayout.setBackgroundResource(R.color.green);
                holder.orderTimeTV.setVisibility(View.GONE);
                break;
            default:
                holder.orderExpressTimeTV.setVisibility(View.GONE);
                holder.orderTypeTV.setText("Отменен");
                holder.orderTypeLayout.setBackgroundResource(R.color.red);
                holder.orderTimeTV.setVisibility(View.GONE);
                break;
        }

    }

    @Override
    public int getItemCount() {
        return mOrders.size();
    }

    public void updateData(List<OrderWithDicts> orders) {

        this.mOrders = orders;
        notifyDataSetChanged();

    }
}


