package com.kolyadruz.kingaqua_android.db.converters;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kolyadruz.kingaqua_android.mvp.models.Product;

import java.util.List;

public class ProductsConverter {

    @TypeConverter
    public String fromList(List<Product> list) {
        return new Gson().toJson(list);
    }

    @TypeConverter
    public List<Product> toList(String data) {
        return new Gson().fromJson(data, new TypeToken<List<Product>>(){}.getType());
    }

}
