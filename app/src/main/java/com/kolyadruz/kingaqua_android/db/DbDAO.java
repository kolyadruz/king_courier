package com.kolyadruz.kingaqua_android.db;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.Courier;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.Order;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.OrderWithDicts;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.PaymentDetail;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.ProductDetail;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.StatusDetail;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.StreetDetail;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.TimeDetail;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface DbDAO {

    @Query("SELECT * FROM courier WHERE hash=:hash")
    Flowable<Courier> getCourierData(String hash);

    @Query("SELECT * FROM courier WHERE id = :id")
    Courier getById(int id);

    @Query("SELECT * FROM orders WHERE status_id IN (2) AND orders.courier_id == :courierId")
    Flowable<List<Order>> getActiveOrders(int courierId);

    @Query("SELECT * FROM orders WHERE status_id IN (3,4,5,6) AND orders.courier_id == :courierId")
    Flowable<List<Order>> getCompletedOrders(int courierId);

    @Query("SELECT * FROM orders WHERE id = :id")
    Flowable<Order> getOrderByID(int id);

    @Transaction
    @Query("SELECT * FROM orders " +
            "WHERE orders.status_id IN (2) AND orders.id IN (:ordersIDs)")
    Flowable<List<OrderWithDicts>> getActiveOrdersWithDict(List<Integer> ordersIDs);

    @Transaction
    @Query("SELECT * FROM orders " +
            "WHERE orders.status_id IN (3,4,5,6) AND orders.id IN (:ordersIDs)")
    Flowable<List<OrderWithDicts>> getCompletedOrdersWithDict(List<Integer> ordersIDs);

    @Transaction
    @Query("SELECT * FROM orders WHERE id = :id")
    Flowable<OrderWithDicts> getOrderWithDictByID(int id);

    @Query("SELECT * FROM products_dict WHERE id in (:productIds)")
    Flowable<List<ProductDetail>> getOrderProducts(List<Integer> productIds);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCourierData(Courier courierEntity);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertOrders(List<Order> orders);

    /*@Query("UPDATE item SET quantity = quantity + 1 WHERE id = :id")
    void updateOrder(Order order);*/

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertProductDeatils(List<ProductDetail> productDetails);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPaymentDeatils(List<PaymentDetail> paymentDetails);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertStatusDeatils(List<StatusDetail> statusDetails);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertTimeDeatils(List<TimeDetail> timeDetails);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertStreetDetails(List<StreetDetail> streetDetails);

}
