package com.kolyadruz.kingaqua_android.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.Courier;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.Order;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.PaymentDetail;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.ProductDetail;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.StatusDetail;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.StreetDetail;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.TimeDetail;

@Database(entities = {
                        Courier.class,
                        Order.class,
                        ProductDetail.class,
                        PaymentDetail.class,
                        TimeDetail.class,
                        StatusDetail.class,
                        StreetDetail.class
                    }, version = 1)

public abstract class AppDatabase extends RoomDatabase {
    public abstract DbDAO courierDAO();
}