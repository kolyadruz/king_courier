package com.kolyadruz.kingaqua_android.di;


import android.content.Context;

import com.kolyadruz.kingaqua_android.di.modules.ContextModule;
import com.kolyadruz.kingaqua_android.di.modules.NetworkModule;
import com.kolyadruz.kingaqua_android.di.modules.RoomModule;
import com.kolyadruz.kingaqua_android.mvp.presenters.ActiveOrdersPresenter;
import com.kolyadruz.kingaqua_android.mvp.presenters.AuthPresenter;
import com.kolyadruz.kingaqua_android.mvp.presenters.CompletedOrdersPresenter;
import com.kolyadruz.kingaqua_android.mvp.presenters.MainPresenter;
import com.kolyadruz.kingaqua_android.mvp.presenters.OrderDetailPresenter;
import com.kolyadruz.kingaqua_android.services.BackgroundLocationService;
import com.kolyadruz.kingaqua_android.ui.orders.fragments.TabContainer;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ContextModule.class, NetworkModule.class, RoomModule.class})
public interface AppComponent {

    Context getContext();

    void inject(AuthPresenter authPresenter);
    void inject(MainPresenter mainPresenter);
    void inject(ActiveOrdersPresenter activeOrdersPresenter);
    void inject(CompletedOrdersPresenter completedOrdersPresenter);
    void inject(OrderDetailPresenter orderDetailPresenter);

    void inject(BackgroundLocationService backgroundLocationService);

}
