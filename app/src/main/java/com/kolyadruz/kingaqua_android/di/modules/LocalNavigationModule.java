package com.kolyadruz.kingaqua_android.di.modules;

import com.kolyadruz.kingaqua_android.di.modules.subnavigation.LocalCiceroneHolder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class LocalNavigationModule {

    @Provides
    @Singleton
    LocalCiceroneHolder provideLocalNavigationHolder() {
        return new LocalCiceroneHolder();
    }

}
