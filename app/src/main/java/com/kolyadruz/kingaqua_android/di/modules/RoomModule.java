package com.kolyadruz.kingaqua_android.di.modules;

import android.content.Context;

import androidx.room.Room;

import com.kolyadruz.kingaqua_android.db.AppDatabase;
import com.kolyadruz.kingaqua_android.db.DbDAO;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RoomModule {

    private AppDatabase appDatabase;

    public RoomModule(Context context) {
        appDatabase = Room.databaseBuilder(context, AppDatabase.class, "database").build();
    }

    @Singleton
    @Provides
    AppDatabase providesRoomDatabase() {
        return appDatabase;
    }

    @Singleton
    @Provides
    DbDAO providesCourierDAO(AppDatabase appDatabase) {
        return appDatabase.courierDAO();
    }


}
