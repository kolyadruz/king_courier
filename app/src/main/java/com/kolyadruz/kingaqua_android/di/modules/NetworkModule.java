package com.kolyadruz.kingaqua_android.di.modules;

import com.kolyadruz.kingaqua_android.app.Api;
import com.kolyadruz.kingaqua_android.mvp.NetworkService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = {ApiModule.class})
public class NetworkModule {

    @Provides
    @Singleton
    public NetworkService provideNetworkService(Api api) {
        return new NetworkService(api);
    }

}
