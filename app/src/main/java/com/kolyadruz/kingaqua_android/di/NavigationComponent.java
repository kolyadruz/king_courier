package com.kolyadruz.kingaqua_android.di;

import com.kolyadruz.kingaqua_android.di.modules.LocalNavigationModule;
import com.kolyadruz.kingaqua_android.di.modules.NavigationModule;
import com.kolyadruz.kingaqua_android.ui.auth.Auth;
import com.kolyadruz.kingaqua_android.ui.orders.activities.MainActivity;
import com.kolyadruz.kingaqua_android.ui.orders.fragments.TabContainer;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {NavigationModule.class, LocalNavigationModule.class})
public interface NavigationComponent {

    void inject(Auth auth);
    void inject(MainActivity mainActivity);

    void inject(TabContainer tabContainer);

}
