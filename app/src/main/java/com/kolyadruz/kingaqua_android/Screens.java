package com.kolyadruz.kingaqua_android;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.google.android.material.tabs.TabLayout;
import com.kolyadruz.kingaqua_android.ui.auth.Auth;
import com.kolyadruz.kingaqua_android.ui.auth.fragments.Password;
import com.kolyadruz.kingaqua_android.ui.auth.fragments.PhoneNumber;
import com.kolyadruz.kingaqua_android.ui.auth.fragments.Restore;
import com.kolyadruz.kingaqua_android.ui.needreplace.gallery.GalleryFragment;
import com.kolyadruz.kingaqua_android.ui.needreplace.send.SendFragment;
import com.kolyadruz.kingaqua_android.ui.needreplace.share.ShareFragment;
import com.kolyadruz.kingaqua_android.ui.needreplace.slideshow.SlideshowFragment;
import com.kolyadruz.kingaqua_android.ui.needreplace.tools.ToolsFragment;
import com.kolyadruz.kingaqua_android.ui.orders.fragments.BottomNavigation;
import com.kolyadruz.kingaqua_android.ui.orders.activities.MainActivity;
import com.kolyadruz.kingaqua_android.ui.orders.fragments.BombOrders;
import com.kolyadruz.kingaqua_android.ui.orders.fragments.CompletedOrders;
import com.kolyadruz.kingaqua_android.ui.orders.fragments.ActiveOrders;
import com.kolyadruz.kingaqua_android.ui.orders.fragments.OrderDetail;
import com.kolyadruz.kingaqua_android.ui.orders.fragments.TabContainer;

import ru.terrakok.cicerone.android.support.SupportAppScreen;

public class Screens {

    public static final class AuthScreen extends SupportAppScreen {

        @Override
        public Intent getActivityIntent(Context context) {
            return new Intent(context, Auth.class);
        }
    }

    public static final class PhoneScreen extends SupportAppScreen {

        @Override
        public Fragment getFragment() {
            return new PhoneNumber();
        }

    }

    public static final class PassScreen extends SupportAppScreen {

        String phoneNumber;

        public PassScreen(String phoneNUmber) {

            this.phoneNumber = phoneNUmber;
        }

        @Override
        public Fragment getFragment() {
            return Password.getNewInstance(phoneNumber);
        }
    }

    public static final class RestoreScreen extends SupportAppScreen {

        String phoneNumber;

        public RestoreScreen(String phoneNUmber) {

            this.phoneNumber = phoneNUmber;
        }

        @Override
        public Fragment getFragment() {
            return Restore.getNewInstance(phoneNumber);
        }
    }

    public static final class MainScreen extends SupportAppScreen {

        @Override
        public Intent getActivityIntent(Context context) {
            return new Intent(context, MainActivity.class);
        }
    }

    public static final class BottomNavScreen extends SupportAppScreen {

        @Override
        public Fragment getFragment() {
            return new BottomNavigation();
        }
    }

    public static final class TabContainerScreen extends SupportAppScreen {

        String tag;

        public TabContainerScreen(String tag) {
            this.tag = tag;
        }

        @Override
        public Fragment getFragment() {
            return TabContainer.getNewInstance(tag);
        }

    }

    public static final class ActiveOrdersScreen extends SupportAppScreen {

        @Override
        public Fragment getFragment() {
            return new ActiveOrders();
        }

    }

    public static final class BombOrdersScreen extends SupportAppScreen {

        @Override
        public Fragment getFragment() {
            return new BombOrders();
        }

    }

    public static final class CompletedOrdersScreen extends SupportAppScreen {

        @Override
        public Fragment getFragment() {
            return new CompletedOrders();
        }

    }

    public static final class OrderDetailScreen extends SupportAppScreen {

        String TabContainerTAG;
        String orderStr;
        String productsStr;

        public OrderDetailScreen(String TabContainerTAG, String orderStr, String productsStr) {
            this.TabContainerTAG = TabContainerTAG;
            this.orderStr = orderStr;
            this.productsStr = productsStr;
        }

        @Override
        public Fragment getFragment() {
            return OrderDetail.getNewInstance(TabContainerTAG, orderStr, productsStr);
        }

    }

    public static final class GalleryScreen extends SupportAppScreen {

        @Override
        public Fragment getFragment() {
            return new GalleryFragment();
        }

    }

    public static final class SlideShowScreen extends SupportAppScreen {

        @Override
        public Fragment getFragment() {
            return new SlideshowFragment();
        }

    }

    public static final class ToolsScreen extends SupportAppScreen {

        @Override
        public Fragment getFragment() {
            return new ToolsFragment();
        }

    }

    public static final class ShareScreen extends SupportAppScreen {

        @Override
        public Fragment getFragment() {
            return new ShareFragment();
        }

    }

    public static final class SendScreen extends SupportAppScreen {

        @Override
        public Fragment getFragment() {
            return new SendFragment();
        }

    }

}
