package com.kolyadruz.kingaqua_android.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5HashUtils {

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        // Create MD5 Hash
        try {
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);

            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexStringBuilder = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexStringBuilder.append(h);
            }

            String hexString = hexStringBuilder.toString();

            return hexString.toUpperCase();

        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
            return null;
        }
    }

}
