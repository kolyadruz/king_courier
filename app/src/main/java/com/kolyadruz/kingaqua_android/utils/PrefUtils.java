package com.kolyadruz.kingaqua_android.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.kolyadruz.kingaqua_android.app.App;

public class PrefUtils {

    private static final String PREF_NAME = "prefs";

    public static SharedPreferences getPrefs() {
        return App.getAppComponent().getContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static SharedPreferences.Editor getEditor() {
        return getPrefs().edit();
    }

}