package com.kolyadruz.kingaqua_android.utils;

import com.google.gson.Gson;
import com.kolyadruz.kingaqua_android.mvp.models.OrderRequestWithOrderData;
import com.kolyadruz.kingaqua_android.mvp.models.roomEntities.Order;

public class OrderToRequestConverter {

    public OrderRequestWithOrderData toRequest(Order order) {

        Gson gson = new Gson();

        String orderStr = gson.toJson(order);

        OrderRequestWithOrderData orderRequestWithOrderData = gson.fromJson(orderStr, OrderRequestWithOrderData.class);

        return orderRequestWithOrderData;

    }

}
